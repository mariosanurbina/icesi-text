FROM bitnami/minideb:stretch
ADD .  /usr/bin 
ADD ./bash /usr/local/bin 
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
RUN apt-get update  && apt-get upgrade -y && apt-get install -y --no-install-recommends apt-utils && apt-get install nano &&  apt-get install -y gnupg
RUN apt-get install -y -q --no-install-recommends \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        git \
        libssl-dev \
        python3-minimal \
        rsync \
        software-properties-common \
        devscripts \
        autoconf \
        ssl-cert \
    && apt-get clean
RUN apt-get install wget -y
RUN apt-get -y update && apt install -y python3-pip &&  apt-get  install -y python3-bs4 &&   pip3 install  beautifulsoup4
WORKDIR /home/pythonApp
